<?php
// +----------------------------------------------------------------------
// | RXThinkCMF敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2023 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace App\Models;

/**
 * 用户权限-模型
 * @author 牧羊人
 * @date: 2023/3/28 12:51
 */
class AdminRomModel extends BaseModel
{
    // 设置数据表
    protected $table = 'admin_rom';

    /**
     * 获取权限菜单
     * @param $roleIds 角色ID集合
     * @param $adminId 用户ID
     * @param $type 类型
     * @param $pid 上级ID
     * @return mixed
     * @author 牧羊人
     * @date: 2023/3/28 12:52
     */
    public function getPermissionMenu($roleIds, $adminId, $type, $pid)
    {
        // 角色权限查询条件
        $map1 = [];
        if ($roleIds) {
            $roleArr = explode(",", $roleIds);
            $map1 = [
                ['r.type', '=', 1],
                ['r.type_id', $roleArr],
            ];
        }
        // 个人独立权限查询条件
        $map2 = [
            ['r.type', '=', 2],
            ['r.type_id', '=', $adminId],
        ];
        $menuModel = new MenuModel();
        $menuList = $menuModel::from("menu as m")
            ->select('m.*')
            ->join('admin_rom as r', 'r.menu_id', '=', 'm.id')
//            ->distinct(true)
            ->where(function ($query) use ($map1, $map2) {
                if (!empty($map1)) {
                    $query->orWhere($map1);
                }
                $query->orWhere($map2);
            })
//            ->whereIn('m.type', [$type, $type + 1])
            ->where('m.pid', '=', $pid)
            ->where('m.status', '=', 1)
            ->where('m.mark', '=', 1)
            ->orderBy('m.pid')
            ->orderBy('m.sort')
            ->get()->toArray();
        if (!empty($menuList)) {
            foreach ($menuList as &$val) {
                if ($val['type'] >= 3) {
                    continue;
                }
                $childList = $this->getPermissionMenu($roleIds, $adminId, $type, $val['id']);
                if (is_array($childList) && !empty($childList)) {
                    $val['children'] = $childList;
                }
            }
        }
        return $menuList;
    }

    /**
     * 获取权限节点列表
     * @param $roleIds 角色ID
     * @param $adminId 用户ID
     * @return array 返回结果
     * @author 牧羊人
     * @date: 2023/3/28 12:52
     */
    public function getPermissionFuncList($roleIds, $adminId)
    {
        // 角色权限查询条件
        $map1 = [];
        if ($roleIds) {
            $roleArr = explode(",", $roleIds);
            $map1 = [
                ['r.type', '=', 1],
                ['r.type_id', $roleArr],
            ];
        }
        // 个人独立权限查询条件
        $map2 = [
            ['r.type', '=', 2],
            ['r.type_id', '=', $adminId],
        ];
        $menuModel = new MenuModel();
        $funcList = $menuModel::from("menu as m")
            ->select('m.*')
            ->join('admin_rom as r', 'r.menu_id', '=', 'm.id')
//            ->distinct(true)
            ->where(function ($query) use ($map1, $map2) {
                if (!empty($map1)) {
                    $query->orWhere($map1);
                }
                $query->orWhere($map2);
            })
            ->where('m.type', '=', 4)
            ->where('m.status', '=', 1)
            ->where('m.mark', '=', 1)
            ->orderBy('m.pid')
            ->orderBy('m.sort')
            ->select('m.permission')
            ->get()->toArray();
        $funcList = array_key_value($funcList, "permission");
        return $funcList;
    }
}
